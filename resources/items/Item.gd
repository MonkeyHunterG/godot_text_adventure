extends Resource
class_name Item


@export var item_name: String = "Item Name"
@export var item_type: Types.ItemTypes = Types.ItemTypes.KEY


# this allows for create new Resource (.tres)
# with Item as base class
# all changes are saved to disk, therefore they stay persistent
# or
# instantiate them via code


var use_value = null
