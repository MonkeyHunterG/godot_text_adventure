extends Resource
class_name Exit


var room: GameRoom = null
var room_other: GameRoom = null
var is_locked: bool = false


func get_other_room(current_room: GameRoom) -> GameRoom:
	if current_room == room:
		return room_other
	elif current_room == room_other:
		return room
	else:
		printerr("This room is not connected to this exit.")
		return null
