extends Node
# go to Project -> Project Settings -> Autoload
# add this file
# enable as Singleton (checkmark)


enum ItemTypes {
	KEY,
	QUEST_ITEM
}

enum directions {
	NORTH = 1, 
	EAST = 2, 
	WEST = 3, 
	SOUTH = 4
}


const COLOR_NPC = Color('#38B6DB')
const COLOR_ITEM = Color('#AF39DB')
const COLOR_SPEECH = Color('#FFFFFF')
const COLOR_LOCATION = Color('#64DB39')
const COLOR_SYSTEM = Color('#DB5F39')


func wrap_system_text(text: String) -> String:
	return '[color=#%s]%s[/color]' %[COLOR_SYSTEM.to_html(false), text]


func wrap_npc_text(text: String) -> String:
	return '[color=#%s]%s[/color]' %[COLOR_NPC.to_html(false), text]


func wrap_item_text(text: String) -> String:
	return '[color=#%s]%s[/color]' %[COLOR_ITEM.to_html(false), text]


func wrap_speech_text(text: String) -> String:
	return '[color=#%s]%s[/color]' %[COLOR_SPEECH.to_html(false), text]


func wrap_location_text(text: String) -> String:
	return '[color=#%s]%s[/color]' %[COLOR_LOCATION.to_html(false), text]
