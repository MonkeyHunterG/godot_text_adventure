extends Resource
class_name Npc


@export var npc_name: String = "NPC Name"
@export_multiline var initial_dialog: String
@export var quest_item: Item
@export var quest_reward: Item
@export_multiline var post_quest_dialog: String


var has_received_quest_item: bool = false
