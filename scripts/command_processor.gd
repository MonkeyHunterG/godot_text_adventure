extends Node


var current_room: GameRoom = null
var current_player = null
var actions = [
	"help/h", "exit", "room/r", "go [direction]", "take [object]", "drop [object]",
	"use [object]", "inventory/i", "talk [object]"]


func initialize(starting_room: GameRoom, player: Node) -> String:
	current_player = player
	return change_room(starting_room)


func process_command(input: String) -> String:
	var words = input.split(" ", false, 0)

	var action: String = words[0].to_lower()
	var object: String = ""
	
	if words[0].to_lower() == "exit":
		get_tree().quit()
	
	if (words.size() > 1):
		object = words[1].to_lower()
	match action:
		"help", "h":
			return help()
		"room", "r":
			return current_room.get_full_room_description()
		"go":
			return go(object)
		"take":
			return take(object)
		"use":
			return use(object)
		"drop":
			return drop(object)
		"inventory", "i":
			return inventory()
		"talk":
			return talk(object)
		_:
			return Types.wrap_system_text("Unrecognized command. Type 'help' for overview")


func change_room(new_room: GameRoom) -> String:
	current_room = new_room
	return current_room.get_full_room_description()


func help() -> String:
	var response: String = ""
	for string in actions:
		response += string + ", "
	return response


func go(object: String) -> String:
	if (object.is_empty()):
		return Types.wrap_system_text("Go where? Direction needed")
	if object in current_room.exits.keys():
		var exit: Exit = current_room.exits[object]
		if exit.is_locked:
			return "The " + Types.wrap_location_text(object) + " is " + Types.wrap_system_text("locked") + " off.Maybe an " + Types.wrap_item_text("item") + " can be of use here."
		
		var change_room_response = change_room(exit.get_other_room(current_room))
		return "\n".join(PackedStringArray([
			"You go " + Types.wrap_location_text(object),
			change_room_response
		]))
	else:
		return "No exit in that direction. Try again."


func take(object: String) -> String:
	if (object.is_empty()):
		return Types.wrap_system_text("Take what? Object needed")
	for item in current_room.items:
		if object == item.item_name.to_lower():
			current_player.take(item)
			current_room.remove_item(item)
			return "You take the " + Types.wrap_item_text(object)
	return "There is no " + Types.wrap_item_text(object) + " in this room. Try again"


func use(object: String) -> String:
	if (object.is_empty()):
		return Types.wrap_system_text("Use what? Object needed")
	for item in current_player.inventory:
		if object == item.item_name.to_lower():
			match item.item_type:
				Types.ItemTypes.KEY:
					for exit in current_room.exits.values():
						if exit.room_other == item.use_value:
							exit.is_locked = false
							current_player.drop(item)
							return "You use the " + Types.wrap_item_text(object)
					return "That item does nothing in this room"
				_:
					printerr("Error: Tried to use an invalid item type")
	return "There is no " + Types.wrap_item_text(object) + " in your inventory. Try again"

func drop(object: String) -> String:
	if (object.is_empty()):
		return Types.wrap_system_text("Drop what? Object needed")
	for item in current_player.inventory:
		if object == item.item_name.to_lower():
			current_player.drop(item)
			for npc in current_room.npcs:
				if npc.quest_item.item_name == object.to_lower():
					npc.has_received_quest_item = true
					if npc.quest_reward != null:
						current_player.take(npc.quest_reward)
					return "You give the " + Types.wrap_item_text(object) + " to the " + Types.wrap_npc_text(npc.npc_name) + "\n" + talk(npc.npc_name)

			current_room.add_item(item)
			return "You drop the " + Types.wrap_item_text(object)
	return "There is no " + Types.wrap_item_text(object) + " in your inventory. Try again"


func inventory() -> String:
	return current_player.get_inventory_list()


func talk(object: String) -> String:
	if current_room.npcs.is_empty():
		return "Nobody to talk to"
	if (object.is_empty()):
		return Types.wrap_system_text("Talk to whom? Object needed")
		
	for npc in current_room.npcs:
		if npc.npc_name == object.to_lower():
			var dialog = npc.post_quest_dialog if npc.has_received_quest_item else npc.initial_dialog
			return Types.wrap_npc_text(npc.npc_name) + ": \"" + Types.wrap_speech_text(dialog) + "\""
	return "There is no " + Types.wrap_npc_text(object) + " here to talk to" 
