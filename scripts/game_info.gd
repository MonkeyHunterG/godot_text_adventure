extends PanelContainer


@export var max_lines_remembered: int = 30


@onready var scroll_container: ScrollContainer = $ScrollContainer
@onready var history_rows: VBoxContainer = $ScrollContainer/HistoryRows
@onready var scrollbar = scroll_container.get_v_scroll_bar()
@onready var input: LineEdit = $"../InputArea/HBoxContainer/Input"


const INPUT_RESPONSE = preload("res://scenes/input/input_response.tscn")


var should_zebra: bool = false


func _ready() -> void:
	scrollbar.changed.connect(_handle_scrollbar_changed)


##### PUBLIC #####
func create_response(response_text: String):
	var response = INPUT_RESPONSE.instantiate()
	_add_response_to_game(response)
	response.set_text(response_text)


func create_response_with_input(response_text: String, input_text: String):
	input.clear()
	var response = INPUT_RESPONSE.instantiate()
	_add_response_to_game(response)
	response.set_text(response_text, input_text)


##### PRIVATE #####
func _handle_scrollbar_changed() -> void:
	scroll_container.scroll_vertical = scrollbar.max_value


func _delete_history_beyond_limit() -> void:
	if (history_rows.get_child_count() > max_lines_remembered):
		var rows_to_forget = history_rows.get_child_count() - max_lines_remembered
		for i in range(rows_to_forget):
			history_rows.get_child(i).queue_free()


func _add_response_to_game(response: Control) -> void:
	history_rows.add_child(response)
	if not should_zebra:
		response.zebrastripe.hide()
	should_zebra = !should_zebra
	_delete_history_beyond_limit()
