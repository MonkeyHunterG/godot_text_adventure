extends Control


@onready var command_processor: Node = $CommandProcessor
@onready var game_info: PanelContainer = $Background/MarginContainer/Rows/GameInfo
@onready var room_manager: Node = $RoomManager
@onready var player: Node = $Player


func _ready() -> void:
	game_info.create_response("Welcome to this retro text adventure. You can type " + Types.wrap_system_text("'help'") + " to see available commands.")
	
	var inital_room_response = command_processor.initialize(room_manager.get_child(0), player)
	game_info.create_response(inital_room_response)


func _on_input_text_submitted(new_text: String) -> void:
	if (new_text.is_empty()):
		return

	var response = command_processor.process_command(new_text)
	game_info.create_response_with_input(response, new_text)


