extends Node


var inventory: Array[Item] = []


func take(item:Item) -> void:
	inventory.append(item)


func drop(item: Item) -> void:
	inventory.erase(item)


func get_inventory_list() -> String:
	if inventory.size() == 0:
		return "Your inventory is empty"
	var item_string: String = ""
	for item in inventory:
		item_string += item.item_name + " "
	return "Inventory: " + item_string
