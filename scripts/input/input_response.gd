extends MarginContainer


@onready var zebrastripe: Panel = $Zebrastripe
@onready var input_history: Label = $Rows/InputHistory
@onready var response_label: RichTextLabel = $Rows/Response


func set_text(response: String, input: String = "") -> void:
	if input == "":
		input_history.queue_free()
	else:
		input_history.text = " > " + input
		
	response_label.text = response
