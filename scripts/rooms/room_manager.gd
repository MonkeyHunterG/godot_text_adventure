extends Node


@onready var starting_house_room: GameRoom = $StartingHouseRoom
@onready var outside_room: GameRoom = $OutsideRoom
@onready var shed_room: GameRoom = $ShedRoom
@onready var gate_room: GameRoom = $GateRoom
@onready var forest_room: GameRoom = $ForestRoom


func _ready() -> void:
	var housekeeper: Npc = load_npc("Housekeeper")
	starting_house_room.add_npc(housekeeper)
	var guard: Npc = load_npc("Guard")
	gate_room.add_npc(guard)
	
	var key: Item = load_item("ShedKey")
	key.use_value = shed_room
	starting_house_room.add_item(key)
	var gate_key: Item = load_item("GateKey")
	gate_key.use_value = forest_room
	#shed_room.add_item(gate_key)
	var guard_sword: Item = load_item("GuardSword")
	#guard_sword.use_value = guard
	shed_room.add_item(guard_sword)
	
	starting_house_room.connect_exit_unlocked("east", outside_room)
	outside_room.connect_exit_locked("north", shed_room)
	outside_room.connect_exit_unlocked("south", gate_room)
	gate_room.connect_exit_locked("forest", forest_room, "gate")


func load_item(item_name: String) -> Resource:
	return load("res://resources/items/" + item_name + ".tres")


func load_npc(npc_name: String) -> Resource:
	return load("res://resources/npcs/" + npc_name + ".tres")
