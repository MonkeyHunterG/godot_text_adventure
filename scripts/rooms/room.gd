@tool
extends PanelContainer
class_name GameRoom


@export var room_name: String = "Room Name": set = set_room_name
@export_multiline var room_description: String = "Room Description": set = set_room_description


var exits: Dictionary = {}
var npcs: Array[Npc] = []
var items: Array[Item] = []


func set_room_name(new_name: String) -> void:
	$MarginContainer/Rows/RoomName.text = new_name
	room_name = new_name
	
	
func set_room_description(new_description: String) -> void:
	$MarginContainer/Rows/RoomDescription.text = new_description
	room_description = new_description


func get_full_room_description() -> String:
	var description = PackedStringArray([get_room_description()])
	var npc_description = get_npc_description()
	if npc_description != "":
		description.append(npc_description)
	var item_description = get_item_description()
	if item_description != "":
		description.append(item_description)
	description.append(get_exit_description())
	return "\n".join(description)


func get_npc_description() -> String:
	if npcs.is_empty():
		return ""
	
	var npc_string: String = ""
	for npc in npcs:
		npc_string += npc.npc_name + " "
	return Types.wrap_npc_text("NPCs") + ": " + npc_string


func get_item_description() -> String:
	if items.is_empty():
		return ""
		
	var item_string: String = ""
	for item in items:
		item_string += item.item_name + " "
	return Types.wrap_item_text("Items") + ": " + item_string


func get_room_description() -> String:
	return "You are " + room_name + ". It is " + room_description + "."


func get_exit_description() -> String:
	return Types.wrap_location_text("Exits") + ": " + ", ".join(PackedStringArray(exits.keys()))


func add_item(item: Item) -> void:
	items.append(item)


func remove_item(item: Item) -> void:
	items.erase(item)


func add_npc(npc: Npc) -> void:
	npcs.append(npc)


func remove_npc(npc: Npc) -> void:
	npcs.erase(npc)


func connect_exit_unlocked(direction: String, room_other: GameRoom, custom_room_other_exit_direction: String = "null") -> void:
	_connect_exits(direction, room_other, false, custom_room_other_exit_direction)


func connect_exit_locked(direction: String, room_other: GameRoom, custom_room_other_exit_direction: String = "null") -> void:
	_connect_exits(direction, room_other, true, custom_room_other_exit_direction)


# private-ish function, denoted by UNDERSCORE
func _connect_exits(direction: String, room_other: GameRoom, is_locked: bool, custom_room_other_exit_direction: String = "null") -> void:
	var exit: Exit = Exit.new()
	exit.room = self
	exit.room_other = room_other
	exit.is_locked = is_locked
	exits[direction] = exit
	if custom_room_other_exit_direction != "null":
		room_other.exits[custom_room_other_exit_direction] = exit
	else:
		match direction:
			"west":
				room_other.exits["east"] = exit
			"east":
				room_other.exits["west"] = exit
			"north":
				room_other.exits["south"] = exit
			"south":
				room_other.exits["north"] = exit
			"inside":
				room_other.exits["outside"] = exit
			"outside":
				room_other.exits["inside"] = exit
